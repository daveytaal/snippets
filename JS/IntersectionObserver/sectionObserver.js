/**
 * Function used for adding class to sections on intersecting
 * @param {string} sSelector The target elements selector
 */
function sectionObserver(sSelector = 'section') {
    if (!IntersectionObserver) {
        return;
    }

    const sections = document.querySelectorAll(sSelector);
    let options = {
        root: null,
        threshold: 0.1
    };

    let observer = new IntersectionObserver(function (entries) {
        entries.forEach(function(entry) {
            if (entry.isIntersecting && !entry.target.classList.contains('observed')) {
                entry.target.classList.add('observed');
            }
        });
    }, options);

    sections.forEach(function (v) {
        v.classList.add('pre-observed');
        observer.observe(v);
    });
}