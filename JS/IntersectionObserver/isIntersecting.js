/**
 * Callback function for element intersecting
 * @param {string} sSelector The main target element
 * @param {function} fCallback The function called on intersecting
 */
function isIntersecting(sSelector, fCallback) {
    new IntersectionObserver(function (entries, observer) {
        entries.forEach(function (entry) {
            if (!entry.isIntersecting) {
                return;
            }

            fCallback();
        });
    }, {
        threshold: 0.1
    })
    .observe(document.querySelector(sSelector));
}