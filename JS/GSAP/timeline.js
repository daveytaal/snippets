/*!
 * GSAP 3.3.1
 * https://greensock.com
 * 
 * @link https://greensock.com/docs/
 */ 

// Set timeline
var tl = gsap.timeline({
    delay: 0,
    defaults: {
        duration: 0.4,
        opacity: 0,
        ease: "power1"
    }
});

// Add each direct child element seperately
document.querySelectorAll('[data-gsap="1"] > *:not(.skip-gsap)').forEach(function (element) {
    tl.from(element, {duration: 0.30, y: 25}, ">-0.1");
});

// Single element
tl.from('[data-gsap="2"]', {y: 25}, ">-0.15");