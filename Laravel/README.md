## Laravel Cheatsheet

# Config
$ php artisan key:generate

# DB
$ php artisan db:seed

$ php artisan migrate

$ php artisan migrate:fresh --seed

$ php artisan migrate:refresh --seed

# Make commands
$ php artisan make:controller [name]

$ php artisan make:controller --model --resource

$ php artisan make:migration [name]

$ php artisan make:model [name]
